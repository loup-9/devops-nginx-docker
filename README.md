# devops-nginx-docker

Ydays project 2022 : containerization of a node application and reverse-proxy utilization.


## Getting started



Use the following command:

```
git clone git@gitlab.com:loup-9/devops-nginx-docker.git
cd devops-nginx-docker/devOps1/
docker-compose up -d
```

On your web browser run :
http://app.127.0.0.1.nip.io/
